package com.chathamfinancial.zipcode.SDET.cucumber;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class Stepdefs {
    private static final String URL = "https://www.behindthename.com/random/";

    private ScenarioContext context;

    public Stepdefs(ScenarioContext context) {
        this.context = context;
    }

    @Given("I am on the name generator")
    public void onTheNameGenerator() {
        this.context.getWebDriver().get(URL);
    }

    @When("^I select a category of (\\w+)$")
    public void selectCategory(String category)  {
        throw new RuntimeException("Finish the select category step");
    }
}