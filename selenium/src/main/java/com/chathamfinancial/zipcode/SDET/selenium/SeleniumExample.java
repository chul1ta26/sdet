package com.chathamfinancial.zipcode.SDET.selenium;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumExample {
    private WebDriver driver;

    @BeforeClass
    public static void setupClass() {
        /**
         *  This is a third party library to download the selenium driver.
         * https://github.com/bonigarcia/webdrivermanager
         * It basically looks up the latest selenium drivers for their respective servers to the systems `.m2/` folder*
         * Then it binds the binary at runtime to running app.
         *
         *   * the `.m2/` directory is where maven caches dependencies
         **/
        WebDriverManager.chromedriver().setup();
    }

    @Before
    public void setupTest() {
        driver = new ChromeDriver();
        driver.get("https://www.google.com/");
    }

    @After
    public void teardown() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Test
    public void test() {

        WebElement element = driver.findElement(By.className("gLFyf"));
        element.sendKeys("Let me google that for you");

        try
        {
            Thread.sleep(5000);
        }
        catch(InterruptedException ex)
        {
            Thread.currentThread().interrupt();
        }
    }
}
